define([
	'require',
	'madjoh_modules/styling/styling',
	'madjoh_modules/session/session',
	'madjoh_modules/ajax/ajax',
	'madjoh_modules/page_manager/tools',
	'madjoh_modules/picture_resize/picture_resize',
	'madjoh_modules/environment/environment',
	'madjoh_modules/custom_events/custom_events'
],
function(require, Styling, Session, AJAX, Tools, PictureResize, Environment, CustomEvents){
	var tools = new Tools();

	var Media = {
		init : function(settings){
			Media.settings = settings;

			if(!document.picturesCache) 	document.picturesCache 		= {};
			if(!document.picturesQueue) 	document.picturesQueue 		= [];
			if(!document.picturesPending) 	document.picturesPending 	= [];
		},

		// PUBLIC INTERFACE
			get : function(media, size, progressCallback){
				var picture = tools.getElement('img', {className : 'media'});
				Media.setSource(picture, media, size, progressCallback);
				return picture;
			},
			getProfilePicture : function(user, size){
				var defaultPictureKey = 'PROFILE_PICTURE_LG';
				if(size && size.width){
						 if(size.width / Styling.getWidth() < 0.15) defaultPictureKey = 'PROFILE_PICTURE_SM';
					else if(size.width / Styling.getWidth() < 0.3) 	defaultPictureKey = 'PROFILE_PICTURE_MD';
				}
				var container = tools.getElement('div', {className : 'profile-picture-container'});
					var defaultPicture = tools.getImage(defaultPictureKey, {className : 'profile-picture-default'});
					var picture = tools.getElement('img', {className : 'profile-picture'});
					var certifiedIcon = tools.getImage('STAR_LABEL', {className : 'certified-icon hidden'});
					tools.appendChildren(container, [defaultPicture, picture, certifiedIcon]);
					Media.setProfilePicture(container, user, size);
				return container;
			},
			setProfilePicture : function(container, user, size){
				var picture = container.querySelector('.profile-picture');
				if(!size) size = {};
				if(!size.width) 	size.width = size.height;
				if(!size.height) 	size.height = size.width;
				if(user && user.photo) 				Media.setSource(picture, user.photo, size);
				else if(user && user.media_photo) 	Media.setSource(picture, user.media_photo, size);
				else if(user && user.facebook_id) 	Media.setFacebookSource(picture, user.facebook_id, size);
				else if(user && user.social_thumb) 	picture.src = user.social_thumb;
				else if(user && user.photo_url) 	picture.src = user.photo_url;

				var certifiedIcon = container.querySelector('.certified-icon');
				if(user && user.certified && certifiedIcon) Styling.removeClass (certifiedIcon, 'hidden');
				else if(certifiedIcon)						Styling.addClass 	(certifiedIcon, 'hidden');
			},
			getProfilePictures : function(users, containerSize, more){
				var picturesContainer = tools.getElement('div', {className : 'profile-pictures-container'});
				Media.setProfilePictures(picturesContainer, users, containerSize, more);
				return picturesContainer;
			},
			setProfilePictures : function(picturesContainer, users, containerSize, more){
				picturesContainer.innerHTML = '';

				if(!users || users.length === 0) return;

				if(users.length === 1){
					Styling.addClass(picturesContainer, 'single-picture');
					Styling.removeClass(picturesContainer, 'multiple-pictures');
					picturesContainer.appendChild(Media.getProfilePicture(users[0], containerSize));
					return;
				}

				Styling.addClass(picturesContainer, 'multiple-pictures');
				Styling.removeClass(picturesContainer, 'single-picture');

				if(containerSize.width) containerSize.width /= 2;
				if(containerSize.height) containerSize.height /= 2;

				for(var i = 0; i < users.length; i++){
					picturesContainer.appendChild(Media.getProfilePicture(users[i], containerSize));
				}
				if(more > 0){
					var moreUsers = tools.getElement('div', {className : 'more-users'});
						moreUsers.appendChild(tools.getTextElement('+' + more, 'div', {rawText : true, className : 'more-users-number'}));
					picturesContainer.appendChild(moreUsers);
				}
			},
			clearProfilePictures : function(picturesContainer){
				picturesContainer.innerHTML = '';
				Media.setProfilePicture(picturesContainer);
			},

		// TOOLS
			getKey : function(media, size){
				var key = '';
				if(media.bucket && media.key) 	key += media.bucket + '_' + media.key;
				else if(media.facebook_id)		key += media.facebook_id;
				if(size.width) 	key += '_w' + size.width;
				if(size.height) key += '_h' + size.height;
				return key;
			},
			clearKey : function(media){
				var keyStart = '';
				if(media.bucket && media.key) 	keyStart += media.bucket + '_' + media.key;
				else if(media.facebook_id)		keyStart += media.facebook_id;
				keyStart += '_';
				for(var key in document.picturesCache){
					if(key.indexOf(keyStart) === 0) delete document.picturesCache[key];
				}
			},
			replaceKey : function(media, dataURL){
				var keyStart = '';
				if(media.bucket && media.key) 	keyStart += media.bucket + '_' + media.key;
				else if(media.facebook_id)		keyStart += media.facebook_id;
				keyStart += '_';
				for(var key in document.picturesCache){
					if(key.indexOf(keyStart) === 0) Media.replaceOneKey(keyStart, key, dataURL);
				}
			},
			replaceOneKey : function(keyStart, key, dataURL){
				var size = key.substr(keyStart.length).split('_');
				for(var i = 0; i < size.length; i++) size[i] = parseFloat(size[i].substr(1));

				size = {
					width 	: size[0],
					height 	: size[1]
				};

				PictureResize.drawUrlInCanvas(dataURL, null, size, function(resizedDataURL){
					document.picturesCache[key].dataURL = resizedDataURL;
				});
			},

		// GET IMAGE
			extensionTypes : ['image', 'audio', 'video'],
			setSource : function(picture, media, size, progressCallback, ext){
				if(!ext) ext = 'src';
				if(!media) return console.log('Media not given');
				if(!size || !(size.width || size.height)) return console.log('Size not defined');

				var key = Media.getKey(media, size);

				if(document.picturesCache[key] && document.picturesCache[key].dataURL){
					picture[ext] = document.picturesCache[key].dataURL;
					if(progressCallback) progressCallback(100);
					return;
				}

				if(!document.picturesCache[key]){
					document.picturesCache[key] = {};
					document.picturesCache[key].promise = Media.insertFetchToQueue(key, media, size, progressCallback);
				}

				document.picturesCache[key].promise.then(function(){
					if(progressCallback) progressCallback(100);
					picture[ext] = document.picturesCache[key].dataURL;
				}, function(error){
					document.picturesCache[key] = null;
					Media.processFetchQueue({key : key});
					if(error && error.request && error.request.xhr && error.request.xhr.status === 403) return;
					CustomEvents.addCustomEventListener(document, 'AJAX_SUCCESS', function(){Media.setSource(picture, media, size, progressCallback);});
				});
			},
			insertFetchToQueue : function(key, media, size, progressCallback){
				return new Promise(function(resolve, reject){
					var imageRequestData = {
						key 				: key,
						media 				: media,
						size 				: size,
						progressCallback 	: progressCallback,
						promiseActions 		: {
							resolve : resolve,
							reject 	: reject
						}
					};

					if(document.picturesPending.length < 10){
						document.picturesPending.push(imageRequestData);
						Media.fetchImage(imageRequestData);
					}else{
						document.picturesQueue.push(imageRequestData);
					}
				});
			},
			processFetchQueue : function(data){
				var index = -1;
				for(var i = 0; i < document.picturesPending.length; i++){
					if(document.picturesPending[i].key === data.key){
						index = i;
						break;
					}
				}

				if(index === -1) return;
				document.picturesPending.splice(index, 1);
				var nextData = document.picturesQueue.shift();
				if(!nextData) return;
				document.picturesPending.push(nextData);
				Media.fetchImage(nextData);
			},
			fetchImage : function(data){
				var url = Media.getS3Url(data.media.bucket, data.media.key);
				var size = {};
				if(data.size.width) 	size.width 	= Math.ceil(data.size.width 	* 1.25);
				if(data.size.height) 	size.height = Math.ceil(data.size.height 	* 1.25);

				AJAX.get(url, {noAuth : true, progressCallback : data.progressCallback}).then(function(result){
					var base64 = Media.base64Encode(result);
					var dataURL = 'data:image/jpeg;base64,' + base64;

					PictureResize.drawUrlInCanvas(dataURL, null, size, function(dataURL){
						document.picturesCache[data.key].dataURL = dataURL;
						data.promiseActions.resolve(dataURL);
						Media.processFetchQueue(data);
					});
				}, data.promiseActions.reject);
			},
			getS3Url : function(bucket, key){
				var env 		= Environment.getEnv();
				var bucketCode 	= Media.settings.buckets[bucket];
				var region 		= Media.settings.bucket_regions[bucketCode];
				var regionUrl 	= Media.settings.region_urls[region];
				var url 		= regionUrl + '/' + bucketCode + '/' + key;
				return url;
			},

		// GET FACEBOOK IMAGE
			setFacebookSource : function(picture, facebook_id, size){
				if(!facebook_id) return console.log('facebook_id not given');
				if(!size || !(size.width || size.height)) return console.log('Size not defined');

				var key = Media.getKey({facebook_id : facebook_id}, size);

				if(document.picturesCache[key] && document.picturesCache[key].dataURL){
					picture.src = document.picturesCache[key].dataURL;
					return;
				}

				if(!document.picturesCache[key]){
					document.picturesCache[key] = {};
					document.picturesCache[key].promise = Media.fetchFacebookImage(key, facebook_id, size);
				}

				document.picturesCache[key].promise.then(function(){
					picture.src = document.picturesCache[key].dataURL;
					return Promise.resolve();
				});
			},
			fetchFacebookImage : function(key, facebook_id, size){
				return new Promise(function(resolve, reject){
					var url = 'https://graph.facebook.com/' + facebook_id + '/picture?type=large';

					if(size.width) 	size.width 	= Math.ceil(size.width 	* 1.25);
					if(size.height) size.height = Math.ceil(size.height * 1.25);

					PictureResize.drawUrlInCanvas(url, null, size, function(dataURL){
						document.picturesCache[key].dataURL = dataURL;
						resolve(dataURL);
					}, true);
				});
			},
			getFbUrl : function(facebook_id){
				return 'https://graph.facebook.com/' + facebook_id + '/picture?height=9999';
			},

			base64Encode : function(str){
				var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
				var out = "", i = 0, len = str.length, c1, c2, c3;
				while (i < len) {
					c1 = str.charCodeAt(i++) & 0xff;
					if (i == len) {
						out += CHARS.charAt(c1 >> 2);
						out += CHARS.charAt((c1 & 0x3) << 4);
						out += "==";
						break;
					}
					c2 = str.charCodeAt(i++);
					if (i == len) {
						out += CHARS.charAt(c1 >> 2);
						out += CHARS.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
						out += CHARS.charAt((c2 & 0xF) << 2);
						out += "=";
						break;
					}
					c3 = str.charCodeAt(i++);
					out += CHARS.charAt(c1 >> 2);
					out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
					out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
					out += CHARS.charAt(c3 & 0x3F);
				}
				return out;
			}
	};

	return Media;
});
